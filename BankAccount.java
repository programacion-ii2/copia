/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.accountholder;

/**
 *
 * @author USUARIO
 */
public class BankAccount {
    
private int accountNumber = 0;
    private Double saldo;
    private AccountHolder accountHolder;

    public BankAccount(Double saldo, AccountHolder accountHolder) {
        this.accountNumber += 1;
        this.saldo = saldo;
        this.accountHolder = accountHolder;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public AccountHolder getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(AccountHolder accountHolder) {
        this.accountHolder = accountHolder;
    }

    public void deposit(Double amount) {

        this.saldo += amount;
    }

    public String withdraw(Double amount) {
        if (amount > 0 && amount <= this.saldo) {
            this.saldo -= amount;
            return "La cantidad a reatirar es " + amount
                    + "\nsaldo disponible:" + this.saldo;
        } else {
            return "Saldo insuficiente";
        }

    }

    public String queryBalance() {
        return "saldo disponible:" + this.saldo;
    }

}

